/**
 * @license
 * Copyright (c) 2014, 2021, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['../accUtils', 'knockout', 'ServicesCalling/servicesConsume', 'appController'],
	function (accUtils, ko, services, app) {
		function DashboardViewModel() {
			var self = this;
			app.pageTitle("Dashboard");
			self.testIcon = ko.observable(false);
			self.cats = ko.observableArray([]);
			function listRequestsCategories() {
				services.getGeneric('genericLookup/getLookup?lookupValueCode=GRP_ID:GRP_NAME:GN_PRIV_GROUPS_T&').then(function (data) {
					services.getGeneric('employeeRequest/allRequests?').then(function (subData) {
						var arr = [];
						for (var item of data.GN_PRIV_GROUPS_T) {
							for (var item1 of subData) {
								if (item.value == item1.requestRoles) {
									arr.push(item)
								}
							}
						}
						var result = arr.reduce((unique, o) => {
							if (!unique.some(obj => obj.label === o.label && obj.value === o.value)) {
								unique.push(o);
							}
							return unique;
						}, []);
						self.cats(result);
						self.testIcon(true);
					}, app.failCbFn);
				}, app.failCbFn);
			}
			self.iconNavigation = function (event, data) {
				app.router.go({path: 'requests', params: {id: data.value, name: data.label}})
			};


			this.connected = () => {
				listRequestsCategories();
				accUtils.announce('Dashboard page loaded.', 'assertive');
				document.title = "Dashboard";
				// Implement further logic if needed
			};

			/**
			 * Optional ViewModel method invoked after the View is disconnected from the DOM.
			 */
			this.disconnected = () => {
				// Implement if needed
			};

			/**
			 * Optional ViewModel method invoked after transition to the new View is complete.
			 * That includes any possible animation between the old and the new View.
			 */
			this.transitionCompleted = () => {
				// Implement if needed
			};
		}

		/*
		 * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
		 * return a constructor for the ViewModel so that the ViewModel is constructed
		 * each time the view is displayed.
		 */
		return DashboardViewModel;
	}
);
