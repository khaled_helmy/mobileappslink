/**
 * @license
 * Copyright (c) 2014, 2021, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['jquery', '../accUtils', 'knockout', 'ServicesCalling/servicesConsume', 'appController', "ojs/ojconveyorbelt", 'ojs/ojinputtext', 'composites/dynamic-form/loader', 'ServicesCalling/drawDynamic', 'composites/icons/loader', 'ojs/ojselectcombobox', 'ojs/ojfilepicker', 'ojs/ojinputnumber', 'ojs/ojswitch', 'ojs/ojcheckboxset', 'ojs/ojdatetimepicker', 'ojs/ojradioset', 'ojs/ojvalidation-base', 'ojs/ojvalidation-number', 'ojs/ojvalidation-datetime'],
	function ($, accUtils, ko, services, app) {
		function DetailsViewModel() {
			var self = this;
			self.trns = app.trns;
			self.isHr = app.isHr;
			self.isIt = app.isIt;
			self.dataConverterToShow = app.dataConverterToShow;
			self.dateConverter = app.dateConverter;
			self.hostName = app.hostName;
			var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';
			self.downloadPath = services.getPath() + 'fileDownload?token=' + token + '&name=';
			self.initrolesTranslations = () => {
				app.trns().selfService = oj.Translations.getTranslatedString('selfService');
				app.trns().Dashboard = oj.Translations.getTranslatedString('Dashboard');
				app.trns().requestInformation = oj.Translations.getTranslatedString('requestInformation');
				app.trns().approvalsInformation = oj.Translations.getTranslatedString('approvalsInformation');
				app.trns().currentApprovals = oj.Translations.getTranslatedString('currentApprovals');
				app.trns().fullname = oj.Translations.getTranslatedString('fullname');
				app.trns().jobname = oj.Translations.getTranslatedString('jobname');
				app.trns().employeenumber = oj.Translations.getTranslatedString('employeenumber');
				app.trns().organization = oj.Translations.getTranslatedString('organization');
				app.trns().decision = oj.Translations.getTranslatedString('decision');
				app.trns().date = oj.Translations.getTranslatedString('date');
				app.trns().jobName = oj.Translations.getTranslatedString('jobName')
				app.trns().personInformation = oj.Translations.getTranslatedString('personInformation');
				app.trns().notifications = oj.Translations.getTranslatedString('notifications');
				app.trns().update = oj.Translations.getTranslatedString('update');
				app.trns().save = oj.Translations.getTranslatedString('save');
				app.trns().cancel = oj.Translations.getTranslatedString('cancel');
				app.trns().preview = oj.Translations.getTranslatedString('preview');
				app.trns().Requestedited = oj.Translations.getTranslatedString('Requestedited');
				app.trns().columnName = oj.Translations.getTranslatedString('columnName');
				app.trns().value = oj.Translations.getTranslatedString('value');
			};

			self.selectLabelValue = app.selectLabelValue;
			self.totalCheck = ko.observable();
			self.inputState = ko.observable(true);

			self.selectLabelValue = function (myArray, nameKey) {
				//            console.log(nameKey);
//                    if (myArray) {
				return app.selectLabelValue(myArray, nameKey);
//                    }else{
//                        return nameKey;
//                    }
			}
			self.iconNavigation = function (route, id, name) {
				app.router.store({
					id: id,
					name: name
				});
				app.router.go(route);
			};
			let uniqueList = [];
			let dupList = [];

//                Array.prototype.contains = function (item) {
//			console.log(this === window)
//                    let filtered_item = this.filter((i) => {
//                        return i.id === item.id
//                    });
//                    return !!filtered_item.length;
//                }

			function contains(list, item) {
				console.log(list)
				console.log(item)
				let filtered_item = list.filter((i) => {
					return i.segmentId === item.segmentId
				});
				return !!filtered_item.length;
			}

			function pushToUniqueList(item) {
				if (!contains(uniqueList, item))
					uniqueList.push(item);
			}

			function pushToDuplicateList(item) {
				if (!contains(dupList, item))
					dupList.push(item);
			}

			self.navigator = app.navigator;
			self.initrolesTranslations();
			self.approvalLength = new ko.observable('');
			self.data = {
				type: 'update'
			};
			self.userInfo = new ko.observable({});
			self.notificationInfo = ko.observable({});
			self.userSeg = new ko.observable([]);
			self.requestSegments = ko.observable([]);
			self.currentSeg = new ko.observable([]);
			self.generalLoopVals = new ko.observable([]);
			self.comments = new ko.observable([]);
			self.requestComment = new ko.observable('');
			self.allowedToApprove = new ko.observable(false);
			self.allowedToCorrect = new ko.observable(true);
			self.empRequestId = ko.observable('');

			function listNotifications(empRequestId) {
				self.userSeg([]);
				var loggedUser = app.getUserInfo()['Login Details'];
				self.allowedToApprove(false);
				self.allowedToCorrect(false);
				var isAdmin = app.isManager();
				var isUpdateRole = app.isUpdateRole();
				var getData = function (data) {
					console.log(data);
					var baseCondition = data.userInfo.status == 'P' && loggedUser.personId.toString() != data.userInfo.userId;
					if (baseCondition) {
						if (data.userInfo.staticApproval) {
							var currentApprovals = data.userInfo.staticApproval.split(",");
							self.allowedToApprove(currentApprovals.includes(loggedUser.personId.toString()));
						}
						if (data.delegations.ASN) {
							self.allowedToApprove(data.delegations.ASN.includes(loggedUser.personId.toString()));
						}
						if (data.delegations.DLG) {
							self.allowedToApprove(self.allowedToApprove() || data.delegations.DLG.includes(loggedUser.personId.toString()));
						}
						self.allowedToApprove(self.allowedToApprove() || isAdmin)
					}
					//                        var baseCorrectCondition = data.userInfo.status == 'T' && loggedUser.userId.toString() == data.userInfo.userId;
					//                        self.allowedToCorrect(baseCorrectCondition || self.allowedToApprove() || isUpdateRole);
//                        self.allowedToCorrect(data.userInfo.allowUpdate);
					self.userInfo(data.userInfo);
					self.userSeg(data.userSeg);
					for (let items of data.decisions) {
						for (let i = 0; i < items.segments.length; i++) {
							if (contains(uniqueList, items.segments[i])) {
								dupList.push(items.segments[i])
							} else {
								uniqueList.push(items.segments[i]);
							}
						}
						items.segments = uniqueList
						uniqueList = [];
						dupList = [];

					}
					var arr = [];
					for (var item of data.decisions) {
						item["creationDate"] = app.dataConverterToShow(item.creationDate);
						arr.push(item);
					}
					self.generalLoopVals(arr);

					var inpVal = "";
					var arr = [];

					for (var item in data.currentSeg.Segement) {
						inpVal = "";
						if (data.currentSeg.Segement[item].type == 'formula') {
							inpVal = (data.currentSeg.Segement[item].formula) ? data.currentSeg.Segement[item].formula : ' ';
						}
						if (data.currentSeg.Segement[item].type == 'note') {
							inpVal = (data.currentSeg.Segement[item].defaultValue) ? data.currentSeg.Segement[item].defaultValue : ' ';
						}
						if (data.currentSeg.Segement[item].type == 'file') {
							inpVal = ko.observableArray([]);
						}


						if (data.currentSeg.Segement[item].valueSets && data.currentSeg.Segement[item].type == 'select') {
							data.currentSeg.Segement[item].valueSets.unshift({
								label: '',
								value: ''
							});
						}
						arr.push({
							"segmentId": data.currentSeg.Segement[item].segmentId,
							"segementName": data.currentSeg.Segement[item].segementName,
							"empValue": inpVal,
							"typeName": data.currentSeg.Segement[item].typeName,
							"type": data.currentSeg.Segement[item].type,
							"required": data.currentSeg.Segement[item].required,
							"valueSets": data.currentSeg.Segement[item].valueSets,
							"display": data.currentSeg.Segement[item].display,
							"formula": data.currentSeg.Segement[item].formula,
							"defaultValue": data.currentSeg.Segement[item].defaultValue,
							"tableArr": data.currentSeg.Segement[item].defaultTableArr

						});
					}

					app.compositUserInfo(data.userInfo);
					app.compositDate(arr);
				};
				services.getGeneric('workFlowNotification/' + empRequestId + '?').then(getData, app.failCbFn);
//				services.getGeneric('workFlowNotification/6290?').then(getData, app.failCbFn);
			}

			self.preview = function () {
				var requestId = self.userInfo().requestId;
				var params = app.router._activeState.params;
				services.addGeneric('employeeRequest/preview?requestId=' + requestId + '&transactionId=' + params.transactionId + '&', self.userSeg()).then(getContent, app.failCbFn);

			}
			var getContent = function (data) {
				document.getElementById("previewContent").innerHTML = data;
				document.getElementById("preview").open();
			}

			self.connected = function () {
				self.userSeg([]);
				app.compComment('');
				var params = app.router._activeState.params;
				var empRequestId = params.empRequestsId;
				listNotifications(empRequestId)
				accUtils.announce('Dashboard page loaded.', 'assertive');
				document.title = "Dashboard";
			};

			self.disconnected = function () {
				app.navigator = function (router, id) {
					app.router.store(id);
					app.router.go(router);
					return true;
				};
			};
			self.transitionCompleted = function () {


			};
			self.personId = app.getUserInfo()['Login Details'].personId;
			self.checkNull = function (val) {
				return val ? val : '';
			}
			self.getInputNumberValue = function (event, data) {
				data.empValue = event.target.value;
			};

			self.getNumeric = function (val) {
				return val ? Number(val) : null;
			}
			self.eatNonNumbers = function (event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				var char = String.fromCharCode(charCode);
				var replacedValue = char.replace(/[^0-9\.]/g, '');
				if (char !== replacedValue) {
					event.preventDefault();
				}
			};
			self.createDataProvider = function (valueSets) {
				return new ArrayDataProvider(valueSets, {
					keyAttributes: 'value'
				});
			};
			self.getCheckBoxValue = function (event, data) {
				data.empValue = event.detail.value.join();
			};
			self.setCheckVal = function (val) {
				return val ? [val] : [];
			};
			self.manager_edit_request = function () {
				self.inputState(false);
			}
			self.inArr = function (segId) {
				return $.inArray(segId, self.outSegments()) && self.totalCheck();
			}
			self.save_edit_request = function () {
				var loggedUser = app.getUserInfo()['Login Details'];
				var final_data = {
					"personId": loggedUser.personId,
					"requestId": self.userInfo().requestId,
					"step": self.userInfo().step,
					"segements": self.userSeg(),
					"requestComment": self.userInfo().comment,
					"empRequestsId": self.userInfo().empRequestsId
				};
				console.log(final_data);
//                    services.addGeneric('employeeRequest?mode=managerEdit&', final_data).then(function () {
//                        app.success_request(app.trns().Requestedited, () => {
//                        });
//                    }, function (xhr) {
//                        app.failCbFn(xhr);
//                    });
				self.inputState(true);
			}
			self.cancel_edit_request = function () {
				self.inputState(true);
			}
			self.controlButtons = ko.computed(function () {
				if (self.inputState()) {
					$('.prepareEdit').show();
					$('.alreadyEdit').hide();
				} else {
					$('.prepareEdit').hide();
					$('.alreadyEdit').show();
				}
			});

//self.addRow = function(data,event,context){
//   console.log($(this));
//    console.log(data);
//    console.log(event);
//    console.log(context)
//    console.log(context.$parents[0].segmentId)
////    context.$parents[0].tableArr.push({})
//    
//}

		}

		/*
		 * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
		 * return a constructor for the ViewModel so that the ViewModel is constructed
		 * each time the view is displayed.
		 */
		return DetailsViewModel;
	}
);
