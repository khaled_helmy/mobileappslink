/**
 * @license
 * Copyright (c) 2014, 2021, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['../accUtils', 'knockout', 'ServicesCalling/servicesConsume', 'appController'],
        function (accUtils, ko, services, app) {
            function DashboardViewModel() {
                var self = this;
                var params = app.router._activeState.params;
                if (params && params.name) {
                    app.pageTitle(params.name + " Requests");
                }
                self.testIcon = ko.observable(true);
                self.icons = ko.observableArray([]);
                function listRequests() {
                    var getRequestCBFN = function (data) {
                        var arr = [];
                        console.log(params)
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].requestRoles == params.id) {
                                arr.push(
                                        {
                                            code: data[i].requestId,
                                            requestName: data[i].requestName,
                                            label: 'c',
                                            value: 'demo-icon-circle-a',
                                            description: data[i].description,
                                            // iconType: app.requestArr()[i].iconName,
                                            iconType: data[i].iconName,
                                            text: data[i].requestName
                                        }
                                );
                            }
                        }
                        self.icons(arr);
                    }
                    services.getGeneric('employeeRequest/allRequests?').then(getRequestCBFN, app.failCbFn);
                }
                self.iconNavigation = function (event, data) {
                    console.log(data)
                    app.router.go({path: 'ss_dynamic', params: {id: data.code, name: data.requestName}})
                };


                this.connected = () => {
                    listRequests();
                    accUtils.announce('Dashboard page loaded.', 'assertive');
                    document.title = "Dashboard";
                    // Implement further logic if needed
                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                this.disconnected = () => {
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                this.transitionCompleted = () => {
                    // Implement if needed
                };
            }

            /*
             * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
             * return a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.
             */
            return DashboardViewModel;
        }
);
