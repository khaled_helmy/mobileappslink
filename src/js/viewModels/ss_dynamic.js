
define(['jquery', 'ojs/ojpagingdataproviderview', 'ojs/ojarraydataprovider', 'knockout', 'jquery', 'appController', 'ServicesCalling/servicesConsume', 'ServicesCalling/drawDynamic', 'ojs/ojknockout-keyset', 'ojs/ojknockout', 'ojs/ojarraydataprovider', 'ojs/ojinputtext', 'composites/icons/loader', 'ojs/ojlabel',
    'ojs/ojselectcombobox', 'ojs/ojtable', 'ojs/ojbutton', 'ojs/ojswitch', 'ojs/ojcheckboxset',
    'ojs/ojdialog', 'ojs/ojpagingcontrol', 'ojs/ojfilepicker', 'composites/dynamic-form/loader', 'ojs/ojinputnumber', 'ojs/ojvalidation-number'],
        function ($, PagingDataProviderView, ArrayDataProvider, ko, $, app, services, buildScreen, keySet) {
            function dynamicScreen() {
                var self = this;
                var params = app.router._activeState.params;
                if (params && params.name) {
                    app.pageTitle(params.name);
                }
                self.pageTitleNavigator = app.navigator;
                self.trns = app.trns;
                self.requestsColumnArray = ko.observableArray();
                self.approvalColomns = ko.observableArray();
                self.SegValues = ko.observable({});
                self.requestName = ko.observable('');
                self.requestId = ko.observable('');
                self.hostName = app.hostName;

                self.valueSets = ko.observable([{colName: "test1", value: ""}, {colName: "test2", value: ""}, {colName: "test3", value: ""}]);
                window.addEventListener('resize', function () {
                    self.pageContentWidth(document.getElementById('pageContent').clientWidth);
                })

                self.getMappingClass = function (type, NOTIFICATION_TYPE) {
                    if (type == 'APPROVED')
                        return 'backgroundGreen';
                    else if (type == 'REJECTED')
                        return 'backgroundRed';
                    else if (type == 'Approval Not Required')
                        return 'backgroundGrey';
                    else if (NOTIFICATION_TYPE == "FYI")
                        return 'backgroundBlue';
                    else
                        return 'backgroundYellow';
                }
                self.getMappingIcon = function (type, NOTIFICATION_TYPE) {
                    if (type == 'APPROVED')
                        return 'icofont icofont-check-circled iconBackgroundGreen';
                    else if (type == 'REJECTED')
                        return 'icofont icofont-close-circled iconBackgroundRed';
                    else if (type == 'Approval Not Required')
                        return 'icofont icofont-scroll-double-down iconBackgroundGrey';
                    else if (NOTIFICATION_TYPE == "FYI")
                        return 'icofont icofont-mail iconBackgroundBlue';
                    else
                        return 'icofont icofont-hour-glass iconBackgroundYellow';
                }

                self.pageContentWidth = ko.observable(document.getElementById('pageContent').clientWidth);
                self.initrequestsTranslations = () => {
                    app.trns().requestName = oj.Translations.getTranslatedString('requestName');
                    app.trns().empId = oj.Translations.getTranslatedString('empId');
                    app.trns().step = oj.Translations.getTranslatedString('step');
                    app.trns().personId = oj.Translations.getTranslatedString('personId');
                    app.trns().CreateRequest = oj.Translations.getTranslatedString('CreateRequest');
                    app.trns().add = oj.Translations.getTranslatedString('add');
                    app.trns().Closereq = oj.Translations.getTranslatedString('Close');
                    app.trns().view = oj.Translations.getTranslatedString('view');
                    app.trns().update = oj.Translations.getTranslatedString('update');
                    app.trns().RFCHistory = oj.Translations.getTranslatedString('RFCHistory');
                    app.trns().approvals = oj.Translations.getTranslatedString('approvals');
                    app.trns().requestName = oj.Translations.getTranslatedString('requestName');
                    app.trns().step = oj.Translations.getTranslatedString('step');
                    app.trns().person = oj.Translations.getTranslatedString('person');
                    app.trns().creationDate = oj.Translations.getTranslatedString('creationDate');
                    app.trns().createdBy = oj.Translations.getTranslatedString('createdBy');
                    app.trns().actions = oj.Translations.getTranslatedString('actions');
                    app.trns().transaction = oj.Translations.getTranslatedString('transaction');
                    app.trns().approverName = oj.Translations.getTranslatedString('approverName');
                    app.trns().delete = oj.Translations.getTranslatedString('delete');
                    app.trns().actionType = oj.Translations.getTranslatedString('actionType');
                    app.trns().actionDate = oj.Translations.getTranslatedString('actionDate');
                    app.trns().segmentName = oj.Translations.getTranslatedString('segmentName');
                    app.trns().oldValue = oj.Translations.getTranslatedString('oldValue');
                    app.trns().newValue = oj.Translations.getTranslatedString('newValue');
                    app.trns().Requestdeleted = oj.Translations.getTranslatedString('Requestdeleted');
                    app.trns().Requestadded = oj.Translations.getTranslatedString('Requestadded');

                };
                self.initrequestsTranslations();
                self.requestsColumnArray = ko.observableArray();
                self.approvalColomns = ko.observableArray();
                self.widthcal = ko.computed(function () {
                    if (this.pageContentWidth() > 500) {
                        self.requestsColumnArray([
//                            {
//                                "headerText": app.trns().requestName,
//                                "headerStyle": "font-weight: 900",
//                                "field": "requestName",
//                            },
//                            {
//                                "headerText": app.trns().step,
//                                "headerStyle": "font-weight: 900",
//                                "field": "step"
//                            },
//                            {
//                                "headerText": app.trns().person,
//                                "headerStyle": "font-weight: 900",
//                                "field": "personFirstName"
//                            },
                            {
                                "headerText": app.trns().createdBy,
                                "headerStyle": "font-weight: 900",
                                "field": "createdBy"
                            },
//                            {
//                                "headerText": app.trns().creationDate,
//                                "headerStyle": "font-weight: 900",
//                                "field": "creationDateStr"
//                            },
                            {

                                "template": "actionsTemplate",
                                "headerStyle": "font-weight: 900",
                                "style": "width: 1px",
                                "field": "status"
                            }
                        ]);
                        self.approvalColomns([
                            {
                                "headerText": app.trns().approvals,
                                "headerStyle": "font-weight: 900",
                                "field": "APPROVAL_NAME"
                            },
                            {
                                "headerText": app.trns().creationDate,
                                "headerStyle": "font-weight: 900",
                                "field": "CREATION_DATEStr"
                            },
                            {
                                "headerText": app.trns().approverName,
                                "headerStyle": "font-weight: 900",
                                "field": "APPROVER_NAME"
                            },
                            {
                                "headerText": app.trns().transaction,
                                "headerStyle": "font-weight: 900",
                                "field": "TRANSACTION_TYPE"
                            }
                        ]);
                    } else {
                        self.requestsColumnArray([
                            {
                                "headerText": "Creation Date",
                                "headerStyle": "font-weight: 900",
                                "field": "creationDateStr"
                            },
                            {
                                "headerText": "Step",
                                "headerStyle": "font-weight: 900",
                                "field": "step"
                            },
                            
                            {

                                "template": "actionsTemplate",
                                "headerStyle": "font-weight: 100",
                                "style": "width: 1px",
                                "field": "status"
                            }
                        ]);
                        self.approvalColomns([
                            {
                                "headerText": "Approval",
                                "headerStyle": "font-weight: 100",
                                "field": "approvalName"
                            },
                            {
                                "headerText": app.trns().transaction,
                                "headerStyle": "font-weight: 100",
                                "field": "transactionType"
                            }
                        ]);
                    }
                    ;
                }, self);
                self.reqInfo = ko.observable({});
                self.showMore = function (event, data) {
			self.reqInfo(data.row)
			document.getElementById("showMore").open()
                }
                var newValueSet = [];
                self.getnewValueSets = function (data) {
                    var applyNewValues = function (getNewValueSetData) {
                        data['NewValues'] = getNewValueSetData;
                        prepareSegments(self.requestId(), app.compositDate(), 'employeeEdit', data);

                        setTimeout(function () {
                            $('select').each(function (event) {
                                var val = $(this).attr('value');
                                console.log(val);

                                if (val) {
                                    $(this).val(val);
                                }
                            });
                        }, 10);
                    }
                    services.getGeneric('employeeRequest/getNewValueSet?requestId=' + self.requestId() + '&personId=' + data.row.personId + '&').then(applyNewValues, app.failCbFn);

                }
                self.returnSelected = app.returnSelected;   
                self.edit_request = function (event, data) {

                    self.getnewValueSets(data)

                    //prepareSegments(self.requestId(), app.compositDate(), 'employeeEdit', data);
                    app.compComment(data.row.requestComment);
                    document.getElementById('addRequestDialog').open();
                }
                self.dynamicApprovalTitle = ko.observable();
                self.show_approvals = function (event, data) {
                    var empReqAppr = function (data) {
                        self.dynamicApprovalTitle(app.trns().approvals + ' | ' + self.requestName());
                        self.approvalsLog(data.approvalslog);
                        var arr = [];
                        for (var item of data.approvalslog) {
                            item["CREATION_DATEStr"] = app.dataConverterToShow(item.CREATION_DATE);
                            item["CREATION_DATE"] = app.dataConverterToShow(item.CREATION_DATE);
                            arr.push(item);
                        }
                        self.approvalsLog({});
                        self.approvalsLog(arr);
                        self.approvalsArray(new ArrayDataProvider(self.approvalsLog()));
                    };
                    services.getGeneric('employeeRequest/employeeRequestApprovals/' + data.row.empRequestsId + '?').then(empReqAppr, app.failCbFn);
                    document.getElementById('showApprovalsDialog').open();
                }
                self.show_history = function (event, data) {
                    if (data.row.hasHistory == 'true') {
                        var empReqHis = function (data) {
                            var arr = [];
                            for (var item of data) {
                                item["editDate"] = app.dataConverterToShow(item.editDate);
                                if (item.type == "date") {
                                    item["newValue"] = app.dataConverterToShow(item.newValue);
                                    item["oldValue"] = app.dataConverterToShow(item.oldValue);
                                }
                                arr.push(item);
                            }
                            self.historyLog(arr);
                        };
                        services.getGeneric('employeeRequest/editEmployeeRequestHistory/' + data.row.empRequestsId + '?').then(empReqHis, app.failCbFn);
                        document.getElementById('showHistoryDialog').open();
                    }
                }
                self.hasHistoryStyle = function (data) {
                    return data.row.hasHistory;
                }
                self.approvalsLog = ko.observable({});
                self.historyLog = ko.observable({});
                self.navigator = app.navigator;
                self.data = {type: 'add'};
                self.id = ko.observable();
                self.newRequestComment = new ko.observable('');
                self.requestSegments = ko.observable([]);
                self.sitesArray = ko.observableArray([]);
                self.approvalsArray = new ko.observable(new ArrayDataProvider([]));
                self.historyArray = new ko.observable(new ArrayDataProvider([]));
                self.requestsPagingDataprovider = new ko.observable(new PagingDataProviderView(new ArrayDataProvider(self.sitesArray, {keyAttributes: 'requestId'})));
                self.addclick = function () {
                    prepareSegments(self.requestId(), app.compositDate(), 'add');
                    app.compComment('');

                    document.getElementById('addRequestDialog').open();
                }

                self.Close = function (event) {
                    document.getElementById('addRequestDialog').close();
                }
//                function listRequests() {
//                    var requestId = app.router._extra.id;
//                    var personId = app.getUserInfo()['Login Details'].personId;
//                    services.getGeneric('employeeRequest?RequestId=' + requestId + '&PersonId=' + personId + '&').then(getRequests, app.failCbFn);
//                }
                var getRequests = function (data) {
                    data.requests.reverse();
                    sessionStorage['tempCheckInserted'] = 0;
                    self.requestsPagingDataprovider(new PagingDataProviderView(new ArrayDataProvider(data.requests, {idAttribute: 'requestId'})));
                };
//                function listSegments() {
//                    var requestId = app.router._extra.id;
//                    services.getGeneric('segements?RequestId=' + requestId + '&step=0&').then(getSegments, app.failCbFn);
//                }

                var getSegments = function (data) {
                    prepareSegments(self.requestId(), data.Segement, 'default');
                };

                self.setTransactionType = function (val) {
                    return self.approvalsLog()[val] ? self.approvalsLog()[val] : '';
                }
                self.createDataProvider = function (valueSets) {
                    return new ArrayDataProvider(valueSets, {keyAttributes: 'value'});
                }
                var getRequests = function (data) {
                    var arr = [];
                    for (var item of data.requests) {
                        item["creationDateStr"] = app.dataConverterToShow(item.creationDate);
                        arr.push(item);
                    }
                    app.dycReqs(data)
                    sessionStorage['tempCheckInserted'] = 0;
                    self.requestsPagingDataprovider(new PagingDataProviderView(new ArrayDataProvider(app.dycReqs().requests, {idAttribute: 'requestId'})));
                };



                var listSegmentsAndRequests = function () {
                    console.log(params)
                    var requestId = params.id;
                    var requestCode = "";
//                    var requestCode = app.router._extra.code ? app.router._extra.code : "";
                    var personId = app.getUserInfo()['Login Details'].personId;
                    services.getGeneric('employeeRequest/listSegmentsAndRequests?lookup=userAdmin&personId=' + personId + '&RequestId=' + requestId + '&RequestCode=' + requestCode + '&step=0&').then(getSegmentsAndRequests, app.failCbFn);
                }
                var getSegmentsAndRequests = function (data) {
                    getSegments(data.seg);
                    getRequests(data.req);
                    app.SegTableValues(data.req.tableSegValues);
                    //self.SegValues(data.req.SegValues);
                    //console.log(data.req.SegValues);
                    //console.log(self.SegValues());
                    data.emp.userAdmin.unshift({label: '', value: ''});
                    app.compositEmpDate(data.emp.userAdmin);
                    app.canAppyOnBehave(data.canApplyOnBehave);
                    app.templateId(data.templateId);
                    self.requestName(data.requestName);
                    self.requestId(data.requestId);
                    app.compositRequestId(data.requestId);
                }
                self.delete_empRequest = function (event, data) {
                    app.modalConfirm(function (confirm) {
                        var jsonData =
                                {
                                    "userId": app.getUserInfo()['Login Details'].userId,
                                    "transactionId": data.row.empRequestsId
                                };
                        services.addGeneric('employeeRequest?mode=delete&', jsonData).then(function (data) {
                            app.success_request(app.trns().Requestdeleted, function () {});
                            getRequests(JSON.parse(data));
                        }, function (xhr) {
                            app.failCbFn(xhr);
                        });
                    });
                };

                self.navigateToEmployee = function (event, data) {

                    var request = {
                        personId: data.row.personId,
                        ladderId: data.row.ladder,
                        mode: "CareerPathProfile"
                    }
                    app.navigator('employeeDetails', request);


                }


                self.connected = function () {
//                    app.templateId(app.router._extra.templateId);
                    self.canDelete = app.isManager();

                    self.showCareerPathBtn = ko.observable('');
//                    self.showCareerPathBtn = app.router._extra.career ? app.router._extra.career : "";
                    self.showCareerPathBtn('hide');

//                    var requestId = app.router._extra.id;
//                    self.requestName(app.router._extra.name);
//                    self.requestId(requestId);
                    $('#module').removeClass();
                    $('#module').addClass('pcoded-main-container SelfServiceContainer');
                    document.getElementById('addRequestDialog').addEventListener("ojClose", function (event) {
                        if (sessionStorage['tempCheckInserted'] == 1) {
                            app.success_request(app.trns().Requestadded, function () {});
                            getRequests(app.dycReqs());
                        }
                        if (sessionStorage['tempCheckInserted'] == 2) {
                            //listRequests();
                        }
                    });

                    listSegmentsAndRequests();
//                    app.promisedRequests(listSegmentsAndRequests);
                };
//                function concatFuncs(){
//                    listSegments();
//                    listRequests();
//                }
                self.setTransactionType = function (val) {
                    return self.approvalsLog()[val] ? self.approvalsLog()[val] : '';
                }
                self.createDataProvider = function (valueSets) {
                    return new ArrayDataProvider(valueSets, {keyAttributes: 'value'});
                }
                self.navigator = function (event, data) {
			app.router.go({path: 'details', params: {empRequestsId: data.row.empRequestsId}})
                }


//                self.btnClass = app.btnClass;
                self.tableClass = ko.computed(function () {
//                    if (self.requestsPagingDataprovider().dataProvider.data.length > 0) {
//                        $('#dynamicRequestsTable').attr('class', app.btnClass($('#dynamicRequestsTable').attr('class')));
//                    }
                })

            }

            function prepareSegments(requestId, data, empValue, data4edit = null) {
                console.log("prepareSegments");
                console.log(app.SegTableValues())
                var userId = app.getUserInfo()['Login Details'].userId;
                var tableArr = [];
                var arr = [];
                var inpVal;

//                var newValueSet = [];
                var oldInpVal;

                for (var item in data) {
                    switch (empValue) {
                        case 'default':
                            console.log("default")
                            inpVal = data[item].defaultValue;
                            if (data[item].valueSets && data[item].type == 'select') {
                                data[item].valueSets.unshift({label: '', value: ''});
                            }
                            tableArr = data[item].defaultTableArr;
                            break;
                        case 'add':
                            console.log("add")
                            inpVal = (data[item].defaultValue) ? data[item].defaultValue : '';
                            app.compositUserInfo({});
                            app.compActionType("add");
                            tableArr = data[item].defaultTableArr;
//                            newValueSet = data[item].valueSets;
                            break;
                        case 'employeeEdit':
                            console.log("employeeEdit")
                            console.log(app.dycReqs());
                            data[item].valueSets = data4edit.NewValues && data4edit.NewValues[data[item].segmentId] ? data4edit.NewValues[data[item].segmentId] : data[item].valueSets
//                            for (var newValueSetItem of data4edit.NewValues) {
//                                if (newValueSetItem.segmentId == data[item].segmentId) {
//                                    newValueSet = newValueSetItem.valueSets;
//                                } else
//                                    newValueSet = data[item].valueSets;
//                            }

                            app.compositUserInfo(data4edit.row);
                            console.log("prepare seg edit user info req img req")
                            console.log(app.compositUserInfo)
                            app.compActionType("edit");
//                            console.log(data4edit);
                            console.log(data);
                            console.log(app.dycReqs());
                            console.log(app.dycReqs().SegValues);
                            console.log(app.SegTableValues());
//
                            inpVal = app.dycReqs().SegValues[data4edit.row.empRequestsId][data[item].segmentId];
                            if (data[item].type == 'formula') {
                                data[item].formula = (inpVal) ? inpVal : ' ';
                            }

                            oldInpVal = app.dycReqs().SegValues[data4edit.row.empRequestsId][data[item].segmentId];//data4edit.row.segValues[data[item].segmentId];
                            if (data[item].type == 'table') {
                                console.log(app.SegTableValues());
                                if (app.dycReqs().tableSegValues[data4edit.row.empRequestsId][data[item].segmentId].length > 0) {
                                    tableArr = app.dycReqs().tableSegValues[data4edit.row.empRequestsId][data[item].segmentId];
                                } else {
                                    tableArr = data[item].defaultTableArr;
                                }
                            }
                            break;
                    }

                    if (data[item].type == 'formula') {
                        inpVal = (data[item].formula) ? data[item].formula : ' ';
                    }
                    if (data[item].type == 'html') {
                        inpVal = (data[item].empValue) ? data[item].empValue : ' ';
                    }
                    if (data[item].type == 'note') {
                        inpVal = (data[item].defaultValue) ? data[item].defaultValue : ' ';
                    }
                    if (data[item].type == 'file') {
                        if (inpVal) {
                            var phArr = [];
                            let i = 0;
                            for (let subItem of inpVal.split(",")) {
                                var obj = {
                                    name: subItem,
                                    size: 500,
                                    type: "file",
                                    index: i
                                }
                                phArr.push(obj);
                                i++;
                            }
                            console.log(phArr)
                            inpVal = ko.observableArray(phArr);
                        } else {
                            inpVal = ko.observableArray([]);
                        }
                    }
                    arr.push({
                        "segmentId": data[item].segmentId,
                        "requestId": data[item].requestId,
                        "step": "0",
                        "segementName": data[item].segementName,
                        "empValue": inpVal,
                        "oldInpVal": oldInpVal,
                        "type": data[item].type,
                        "createdBy": userId,
                        "typeName": data[item].typeName,
                        "approval": data[item].approval,
                        "valueSets": data[item].valueSets,
                        "valueSetId": data[item].valueSetId,
                        "required": data[item].required,
                        "display": data[item].display,
                        "depend": data[item].depend,
                        "prompt": data[item].prompt,
                        "formula": data[item].formula,
                        "defaultValue": data[item].defaultValue,
                        "defaultTableArr": data[item].defaultTableArr,
                        "tableArr": tableArr,

                    });
                }

                console.log(arr);

                app.compositDate(arr);


            }



            return new dynamicScreen();
        });