define(['knockout', 'jquery', 'ServicesCalling/servicesConsume', 'appController', 'ojs/ojmessages', 'ojs/ojselectcombobox'],
	function (ko, $, services, app, moduleUtils) {

		function LoginViewModel() {
			var self = this;
			self.userName = ko.observable("");
			self.password = ko.observable("");
			self.language = ko.observable(oj.Config.getLocale());
			self.langs = [
				{
					label: 'English',
					value: 'US'
				}, {
					label: 'العربية',
					value: 'AR'
				}];
			self.loginOnEnterKey = function (event) {
				if (event.key == 'Enter') {
					self.signInBtn();
				}
			}

			self.signInBtn = function () {
				var errors = 0;
				$("#username").css("border-color", "#ccc");
				$("#password").css("border-color", "#ccc");
				if (!self.userName()) {
					$("#username").css("border-color", "#f00");
					errors++;
				}
				if (!self.password()) {
					$("#password").css("border-color", "#f00");
					errors++;
				}
				if (errors === 0) {
					app.loginRequest(self.userName(), self.password());
				}

			}
//                self.setLocale = function (data) {
//                    var lang = data.detail.value;
//                    localStorage["selectedLanguage"] = lang;
//                    oj.Config.setLocale(lang, function () {
//                        app.switchLang(lang)
//                        initTranslations();
//                    });
//                };
//                function initTranslations() {
//                    app.trns().loginLabel = oj.Translations.getTranslatedString('loginLabel');
//                    app.trns().rememberMe = oj.Translations.getTranslatedString('rememberMe');
//                    app.trns().forgotPassword = oj.Translations.getTranslatedString('forgotPassword');
//                    app.trns().userNameLable = oj.Translations.getTranslatedString('userNameLable');
//                    app.trns().password = oj.Translations.getTranslatedString('password');
//                }
			self.connected = function () {
//                    initTranslations();
//                    app.logout()
				app.loginRequest("portal7", "123456");
				$('#module').removeClass();
				$('#module').addClass('pcoded-main-container login-background');
			};
			self.disconnected = function () {
				// Implement if needed
			};
			self.navigator = app.navigator;


		}
		return new LoginViewModel();
	}
);
