define(['knockout', 'jquery', 'ServicesCalling/servicesConsume', 'appController', 'ojs/ojbutton', 'ojs/ojmessages', 'ojs/ojselectcombobox', 'ojs/ojinputtext', 'ojs/ojbutton', 'ojs/ojdialog', 'ojs/ojlabel', 'ojs/ojradioset', 'ojs/ojformlayout', "ojs/ojselectsingle", "ojs/ojinputnumber", "ojs/ojinputnumber", "ojs/ojdatetimepicker", "ojs/ojcheckboxset", "ojs/ojselectcombobox", "ojs/ojfilepicker", "ojs/ojconveyorbelt", "ojs/ojfilmstrip"],
        function (ko, $, services, app, moduleUtils) {

            function LoginViewModel() {
                var self = this;
                var params = app.router._activeState.params;
                    app.pageTitle("New Request");
                self.trns = app.trns;
                self.navigator = app.navigator;
                self.userName = ko.observable("");
                self.password = ko.observable("");
                self.language = ko.observable(oj.Config.getLocale());
                self.langs = [
                    {
                        label: 'English',
                        value: 'US'
                    }, {
                        label: 'Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©',
                        value: 'AR'
                    }];




                function initTranslations() {
                    app.trns().loginLabel = oj.Translations.getTranslatedString('loginLabel');
                    app.trns().rememberMe = oj.Translations.getTranslatedString('rememberMe');

                }


                $(document).ready(function () {
                    function updateClonedInput(index, element, param, parentClass) {
                        console.log(index);
                        $(element).appendTo(parentClass).attr("id", "clonedInput" + index);
                        if (param) {
                            $(element).find(':input').val('');
                        }

                    }
                    $(document).off().on("click", ".clonedInput .clone", function (e) {
                        e.preventDefault();
                        var parentClass = $(this).parent().parent().parent();
                        var cloneIndex = parentClass.find('.clonedInput').length + 1;
                        var new_Input = $(this).closest('.clonedInput').length ? $(this).closest('.clonedInput').clone() : $(".clonedInput:last").clone();
                        updateClonedInput(cloneIndex, new_Input, true, parentClass);
                    });
                    $(document).on("click", ".clonedInput .remove", function (e) {
                        var parentClass = $(this).parent().parent().parent();
                        console.log(parentClass);
                        if (parentClass.find('.clonedInput').length > 1) {
                            e.preventDefault();
                            $(this).parents(".clonedInput").remove();
                            parentClass.find('.clonedInput').each(function (cloneIndex, clonedElement) {
                                updateClonedInput(cloneIndex + 1, clonedElement, false, parentClass);
                            })

                        }
                    });
                });





                self.connected = function () {
                    initTranslations();
                    $('#module').removeClass();
                    $('#module').addClass('pcoded-main-container login-background');
                };



                self.disconnected = function () {
                    // Implement if needed
                };



            }
            return new LoginViewModel();
        }
);
