define(['jquery','knockout', 'appController'],
       function ($,ko, app) {
    function model(context) {
        var self = this;
        self.connected = function () {
            app.promisedRequests(function(){});
            $('.outer-ellipsis i').on('click',function(e){
                app.compositIconDate(e.delegateTarget.classList.value);
                document.getElementById('iconsDialog').close();
            });
        };
    }
    return model;
});