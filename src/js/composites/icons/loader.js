define(['ojs/ojcore', 'text!./icons.html', './comp','ojs/ojcomposite'], 
       function (oj, view, viewModel) 
       {
    oj.Composite.register('icons-form',{
        view: view,
        viewModel: viewModel,
        metadata: {
            "name": "icons-form",
            "description": "My custom link component",
            "version": "1.0.0",
            "properties": {
                "data": {
                    "type": "object"
                }
            }
        }
    });
}
      );