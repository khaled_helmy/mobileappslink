define(['ojs/ojcore', 'text!./icons.html', './comp','ojs/ojcomposite'], 
       function (oj, view, viewModel) 
       {
    oj.Composite.register('dynamic-popup',{
        view: view,
        viewModel: viewModel,
        metadata: {
            "name": "dynamic-popup",
            "description": "My custom link component",
            "version": "1.0.0",
            "properties": {
                "data": {
                    "type": "object"
                }
            }
        }
    });
}
      );