define(['jquery', 'ojs/ojcore', 'ojs/ojarraydataprovider', 'knockout', 'appController', 'ServicesCalling/servicesConsume', 'composites/dynamic-form/loader', 'ojs/ojdialog'],
        function ($, oj, ArrayDataProvider, ko, app, services) {
            function model(context) {
                var self = this;
                self.trns = app.trns;
                    app.trns().delegate = oj.Translations.getTranslatedString('delegate');
                window.addEventListener('resize', function () {
                    self.pageContentWidth(document.getElementById('pageContent').clientWidth);
                })
                self.pageContentWidth = ko.observable(document.getElementById('pageContent').clientWidth);
                
                self.newRequestComment = app.compComment;
                self.CreateRequest = ko.observable(oj.Translations.getTranslatedString('dynamic.CreateRequest'));
                var loggedUser = app.getUserInfo()['Login Details'];
                self.personId = loggedUser.personId;
                if (loggedUser.signature) {
                    self.signature = loggedUser.signature
                } else {
                    self.signature = ""
//                    self.signature = "css/images/2454_logo.png"
                }
           

            }
            return model;
        });

