define(['jquery', 'knockout', 'ojs/ojpagingdataproviderview', 'appController', 'ServicesCalling/servicesConsume', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojdatacollection-utils', 'ojs/ojvalidation-base', 'ojs/ojknockout', 'ojs/ojtable', 'ojs/ojinputtext', 'ojs/ojbutton', 'ojs/ojcheckboxset', 'ojs/ojdialog', 'ojs/ojlabel', 'ojs/ojselectcombobox', 'ojs/ojswitch', 'ojs/ojpagingcontrol', 'composites/icons/loader', 'ojs/ojknockout-validation', 'ojs/ojformlayout', 'ojs/ojmessages', 'ojs/ojdatetimepicker', 'composites/dynamic-form/loader', 'ServicesCalling/drawDynamic', 'ojs/ojvalidation-number', "ojs/ojconveyorbelt"],
        function ($, ko, PagingDataProviderView, app, services, Bootstrap, ArrayDataProvider, DataCollectionEditUtils) {

            function model(context) {
                var self = this;
                self.trns = app.trns;
                app.trns().delegate = oj.Translations.getTranslatedString('delegate');
                app.trns().reassign = oj.Translations.getTranslatedString('reassign');
                app.trns().returnforCorrection = oj.Translations.getTranslatedString('returnforCorrection');
                app.trns().approve = oj.Translations.getTranslatedString('approve');
                app.trns().fullname = oj.Translations.getTranslatedString('fullname');
                app.trns().thisFieldIsRequired = oj.Translations.getTranslatedString('thisFieldIsRequired');
                app.trns().reject = oj.Translations.getTranslatedString('reject');
                app.trns().type = oj.Translations.getTranslatedString('type');
                app.trns().size = oj.Translations.getTranslatedString('size');
                app.trns().date = oj.Translations.getTranslatedString('date');
                app.trns().update = oj.Translations.getTranslatedString('update');
                app.trns().save = oj.Translations.getTranslatedString('save');
                app.trns().chooseUser = oj.Translations.getTranslatedString('chooseUser');
                app.trns().submit = oj.Translations.getTranslatedString('submit');
                app.trns().cancel = oj.Translations.getTranslatedString('cancel');
                app.trns().comment = oj.Translations.getTranslatedString('comment');
                app.trns().actions = oj.Translations.getTranslatedString('actions');
                app.trns().preview = oj.Translations.getTranslatedString('preview');
                app.trns().columnName = oj.Translations.getTranslatedString('columnName');
                app.trns().value = oj.Translations.getTranslatedString('value');
                window.addEventListener('resize', function () {
                    self.pageContentWidth(document.getElementById('pageContent').clientWidth);
                })

                self.pageContentWidth = ko.observable(document.getElementById('pageContent').clientWidth);
                self.transactionType = new ko.observable('');
                self.newRequestComment = new ko.observable('');
                self.base64 = new ko.observable('');
                self.empId = new ko.observable('');
                self.actionDialog = new ko.observable('');
                self.empRequired = new ko.observable(false);
                self.fileExtention = new ko.observable('');
                self.newRequestComment = app.compComment;
                self.CreateRequest = ko.observable(oj.Translations.getTranslatedString('dynamic.CreateRequest'));
                var loggedUser = app.getUserInfo()['Login Details'];
                self.personId = loggedUser.personId;
                if (loggedUser.signature) {
                    self.signature = loggedUser.signature
                } else {
                    self.signature = ""
//                    self.signature = "css/images/2454_logo.png"
                }



                self.setRequiredColumn = function (required) {

                    if (required == "Y") {
                        return "required";
                    } else
                        return "";


                }


                function validateTableColumn() {

                    $('.clonedInput .required').each(function () {

                        if ($(this).val() == "") {
                            $(this).parent().find('.colError').show();
//                            $(this).css("border-color", "red");
                        } else {
//                            $(this).css("border-color", "#e5e6e6");
                            $(this).parent().find('.colError').hide();

                        }
                    });

                    return "";
                }



                self.selectLabelValue = app.selectLabelValue;
                self.returnSelected = app.returnSelected;

                self.newRequest = function () {
                    validateTableColumn();
                    var error = validRequiredSegements(self.requestSegments());
                    console.log(self.empRequired());
                    if (self.empRequired() && self.empId() == '') {
                        error++;
                        $('.empReq').show();
                    } else {
                        $('.empReq').hide();
                    }
                    if (error == 0) {
                        if (app.compositUserInfo().empRequestsId) {
                            self.create_request('employeeEdit');
                        } else {
                            //for loop on segs
                            //if type == table
                            var EmpArr = [];
                            $('.columnCont .j-row').each(function (item) {
//                        
                                var name = $(this).find('.colName').val();
                                var value = $(this).find('.columnValue').val();
                                if (value && name) {
                                    var obj = {colName: name, colValue: value};
                                    EmpArr.push(obj);
                                }
                            });
                            console.log(EmpArr);
                            //end
                            //end
                            var requestId = app.compositRequestId();
                            console.log("requestId >> " + requestId);
                            //var requestId = app.router._extra.id;
                            var updtArr = [];
                            for (var item of self.requestSegments()) {
                                console.log(item.type);
                                if (item.type == 'table') {
                                    var arr = [];
                                    $('.columnCont_' + item.segmentId + ' .j-row').each(function (item) {
                                        $(this).find('.input').each(function () {
                                            var obj = {};
                                            console.log($(this).children()[0].classList[0].split("_"))
                                            var rowNo = ($(this).parent().parent()[0].id).replace('clonedInput', '');
                                            var segId = $(this).children()[0].classList[0].split("_")[1];
                                            var colId = $(this).children()[0].classList[0].split("_")[2];
                                            var value = "";

                                            value = $(this).find('select').val() ? $(this).find('select').val() : $(this).find(':input').val();
                                            console.log(($(this)));
                                            console.log(value);


//                                            if (value != "") {
                                            obj['value'] = value;
                                            obj['colId'] = colId;
                                            obj['rowNo'] = rowNo;
                                            obj['segId'] = segId;
//                                            }
                                            arr.push(obj);
                                        });
                                    });
                                    console.log(arr)
                                    item.empValue = arr;
                                }
                                if (item.segmentId) {
                                    updtArr.push(item)
                                }
                            }

                            console.log(updtArr);
                            var final_data = {
                                "requestId": requestId,
                                "personId": self.empId() != '' ? self.empId() : loggedUser.personId,
                                "step": "0",
                                "segements": updtArr,
                                "requestComment": self.newRequestComment(),
//                               
                            };
                            console.log(final_data);
                            services.addGeneric('employeeRequest?mode=ADD&', final_data).then(func_success, function (xhr) {
                                app.failCbFn(xhr, fail);
                            });
                            for (var item in self.requestSegments()) {
                                for (var item1 in defaults) {
                                    if (self.requestSegments()[item].prompt == item1) {
                                        if (self.requestSegments()[item].type == "note") {
                                            self.requestSegments()[item].defaultValue = defaults[item1];
                                        } else if (self.requestSegments()[item].type == "formula") {
                                            self.requestSegments()[item].formula = defaults[item1];
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
                self.preview = function () {
                    var requestId = app.router._activeState.params.id;
                    services.addGeneric('employeeRequest/preview?requestId=' + requestId + '&transactionId=0&', self.requestSegments()).then(getContent, app.failCbFn);
                }
                var getContent = function (data) {
                    document.getElementById("preDev").innerHTML = data;
                    $("#segDev").animate({width: "60%"}, 700);
                    $("#preDev").animate({width: "40%"}, 700);
                }


                function fail(responseText) {
                    if (responseText[0] == 'WARNING') {
                        $("#customErrorMassege").attr("class", "sa-icon sa-warning");
                        app.failModalConfirm(function (confirm) {
                            if (confirm) {
                                if (responseText[responseText.length - 1] === 'empEditRes') {
                                    var final_data = {
                                        "personId": loggedUser.personId,
                                        "requestId": app.compositUserInfo().requestId,
                                        "step": app.compositUserInfo().step,
                                        "segements": self.requestSegments(),
                                        "requestComment": self.newRequestComment(),
                                        empRequestsId: responseText[3]
                                    };
                                    services.addGeneric('employeeRequest?mode=employeeEditConfirm&', final_data).then(function () {
                                        app.success_request("Request has been saved successfully", function () {});
                                    }, function (xhr) {
                                        app.failCbFn(xhr, fail);
                                    });
                                    document.getElementById("addRequestDialog").close();
                                } else {
                                    services.addGeneric('employeeRequest?mode=CONFIRM&', {personId: app.getUserInfo()['Login Details'].personId, empRequestsId: responseText[3]}).then(func_success, function (xhr) {
                                        app.failCbFn(xhr, fail);
                                    });
                                    document.getElementById("addRequestDialog").close();
                                }
                            } else {
                                if (responseText[responseText.length - 1] === 'empEditRes') {
                                    document.getElementById("addRequestDialog").close();
                                } else {
                                    services.addGeneric('employeeRequest?mode=CANCEL&', {personId: app.getUserInfo()['Login Details'].personId, empRequestsId: responseText[3]}).then(function () {
//                                    app.success_request("Request has been saved successfully", function () {});
                                    }, function (xhr) {
                                        app.failCbFn(xhr, fail);
                                    });
                                }
                            }
                            sessionStorage['tempCheckInserted'] = 2;
                        }, responseText[1], responseText[2])
                    } else {

//                            $("#customErrorMassege").show();
//                            $(".sa-success").hide();
//                            $(".sa-info").hide();
                        $("#msg").text(responseText[1]);
                        $("#sub_msg").text(responseText[2]);
                        if (responseText[0] == 'ERROR') {
                            $("#customErrorMassege").attr("class", "sa-icon sa-error");
                        } else {
                            $("#customErrorMassege").attr("class", "sa-icon sa-info");
//                                $(".sa-info").show();
                        }
                        $("#msg_modal").show();
                        $("#background").show();
                        sessionStorage['tempCheckInserted'] = 2;
                        //document.getElementById("addRequestDialog").close();
                    }

                }
                function func_success(data) {
                    var jsonData = JSON.parse(data);
                    app.dycReqs({});
                    app.dycReqs(jsonData);
                    document.getElementById('addRequestDialog').close();
                    sessionStorage['tempCheckInserted'] = 1;
                }
                self.setCheckVal = function (val) {
                    return val ? [val] : [];
                };
                self.create_request = function (response) {

                    console.log("hazem>>>" + response)
                    console.log(self.requestSegments())
                    console.log(app.compositUserInfo());
//                    app.compModalConfirm(function (confirm) {
                        validateTableColumn();
                        var error = validRequiredSegements(self.requestSegments());
                        if (error == 0 || response == "CORRECT") {



                            var updtArr = [];
                            for (var item of self.requestSegments()) {
                                console.log(item.type);
                                if (item.type == 'table') {
                                    var arr = [];
                                    $('.columnCont_' + item.segmentId + ' .j-row').each(function (item) {
                                        $(this).find('.input').each(function () {
                                            var obj = {};
                                            console.log($(this).children()[0].classList[0].split("_"))
                                            var rowNo = ($(this).parent().parent()[0].id).replace('clonedInput', '');
                                            var segId = $(this).children()[0].classList[0].split("_")[1];
                                            var colId = $(this).children()[0].classList[0].split("_")[2];
                                            var value = "";

                                            value = $(this).find('select').val() ? $(this).find('select').val() : $(this).find(':input').val();
                                            console.log(($(this)));
                                            console.log(value);


//                                            if (value != "") {
                                            obj['value'] = value;
                                            obj['colId'] = colId;
                                            obj['rowNo'] = rowNo;
                                            obj['segId'] = segId;
//                                            }
                                            arr.push(obj);
                                        });
                                    });
                                    console.log(arr)
                                    item.empValue = arr;
                                }
                                if (item.segmentId) {
                                    updtArr.push(item)
                                }
                            }

                            var final_data = {
                                "personId": loggedUser.personId,
                                "requestId": app.compositUserInfo().requestId,
                                "step": app.compositUserInfo().step,
//                                "segements": self.requestSegments(),
                                "segements": updtArr,
                                "requestComment": self.newRequestComment(),
                                "empRequestsId": app.compositUserInfo().empRequestsId
                            };
                            services.addGeneric('employeeRequest?mode=' + response + '&', final_data).then(function () {
                                app.success_request("Request has been saved successfully", function () {
                                    app.navigator( 'dashboard');
                                });
                            }, function (xhr) {
                                app.failCbFn(xhr, fail);
                            });
                            for (var item in self.requestSegments()) {
                                for (var item1 in defaults) {
                                    if (self.requestSegments()[item].prompt == item1) {
                                        if (self.requestSegments()[item].type == "note") {
                                            self.requestSegments()[item].defaultValue = defaults[item1];
                                        } else if (self.requestSegments()[item].type == "formula") {
                                            self.requestSegments()[item].formula = defaults[item1];
                                        }
                                    }
                                }
                            }
                        }
//                    }, response);
                };
                self.handleCompActionType = ko.computed(function () {
                    if (app.compActionType() == "add") {
                        $('.applyOnBehave').show();
                    } else {
                        $('.applyOnBehave').hide();
                    }
                })
                self.createDataProvider = function (valueSets) {
                    return new ArrayDataProvider(valueSets, {keyAttributes: 'value'});
                };
                context.props.then(function (properties) {

                    self.dateConverter = app.dateConverter;
                    self.transactionType = properties.data.type;
                    console.log("transactionType>>>>>>" + self.transactionType);
                    self.requestSegments = app.compositDate;
                    self.empDate = app.compositEmpDate;
                    self.canAppyOnBehave = app.canAppyOnBehave;
                    self.templateId = app.templateId;
                    self.btnClass = app.btnClass;
                    self.onBehave = app.isHr() || app.isManager();
                    console.log("isHr == " + app.isHr());
                    console.log("isManager == " + app.isManager());
                    console.log("onBehave == " + self.onBehave);
                    $(document).ready(function () {
//                        displayRemove("");
                        function updateClonedInput(index, element, param, parentClass) {
                            console.log(index);
                            $(element).appendTo(parentClass).attr("id", "clonedInput" + index);
                            if (param) {
                                $(element).find(':input').val('');
                            }
//                            $(element).find(">:first-child").next().attr("id", "cs_product" + index + "_button");
//                            displayRemove(parentClass);
                        }
//
//                        function displayRemove(parentClass) {
//                            if (parentClass) {
//                                if (parentClass.find('.clonedInput').length === 1) {
//                                    $('.remove').hide();
//                                } else {
//                                    $('.remove').show();
//                                }
//                            } else {
//
//                                if ($('.clonedInput').length === 1) {
//                                    $('.remove').hide();
//                                } else {
//                                    $('.remove').show();
//                                }
//                            }
//                        }

                        //worked jquery
                        $(document).off().on("click", ".clonedInput .clone", function (e) {
                            e.preventDefault();
                            var parentClass = $(this).parent().parent().parent();
                            var cloneIndex = parentClass.find('.clonedInput').length + 1;
                            var new_Input = $(this).closest('.clonedInput').length ? $(this).closest('.clonedInput').clone() : $(".clonedInput:last").clone();
                            updateClonedInput(cloneIndex, new_Input, true, parentClass);
                        });
                        $(document).on("click", ".clonedInput .remove", function (e) {
                            var parentClass = $(this).parent().parent().parent();
                            console.log(parentClass);
                            if (parentClass.find('.clonedInput').length > 1) {
                                e.preventDefault();
                                $(this).parents(".clonedInput").remove();
                                parentClass.find('.clonedInput').each(function (cloneIndex, clonedElement) {
                                    updateClonedInput(cloneIndex + 1, clonedElement, false, parentClass);
                                })

                            }
                        });
                    });
                });





                self.checkNull = function (val) {
                    return val ? val : '';
                };
                function validRequiredSegements(arr) {
                    var error = 0;
                    $('.required').each(function () {
                        console.log($(this).prop("tagName"));
                        if ($(this).prop("tagName") == 'OJ-FILE-PICKER') {
                            var countPhotos = $(this).parent().find(".jFiler-items-list").children().length;
                            if (countPhotos > 0) {
                                $(this).parent().parent().find('.popover-valid').hide();
                            } else {
                                error++;
                                $(this).parent().parent().find('.popover-valid').show();
                            }

                        } else if ($(this).prop("tagName") !== 'OJ-LABEL') {
                            if ($(this).val()) {
                                $(this).parent().children('span').hide();
                            } else {
                                error++;
                                $(this).parent().children('span').show();
                                error++;
                            }
                        }
                    });
                    return error;
                }
                self.getCheckBoxValue = function (event, data) {
                    data.empValue = event.detail.value.join();
                    self.getDepend(event, data);
                };
                self.round = function (val) {
                    return Math.round(val / 1000);
                }
                self.delete_file = function (event, file, parent) {
                    for (var item in parent.$parent.empValue()) {
                        if (parent.$parent.empValue()[item].index == file.index) {
                            parent.$parent.empValue.splice(item, 1);
                            break
                        }
                    }
                    if (parent.$parent.attachments)
                        delete parent.$parent.attachments[file.index]
                    if (parent.$parent.fileInfo) {
                        //new photos
                        delete parent.$parent.fileInfo[file.index]

                    } else {
                        //old photos to be deleted 
                        var arrPh = parent.$parent.oldInpVal.split(",");
                        let index = arrPh.indexOf(file.name);
                        if (index > -1) {
                            arrPh.splice(index, 1);
                        }
                        parent.$parent.oldInpVal = arrPh.join(",");
                    }



                }


                self.selectListener = function (event, data) {
                    var files = event.detail.files;
                    data.fileInfo = data.fileInfo ? data.fileInfo : {};
                    data.attachments = data.attachments ? data.attachments : {};
                    var startVal = data.empValue().length ? data.empValue()[data.empValue().length - 1].index + 1 : 0;
                    for (var i = 0; i < files.length; i++) {
                        var fileInfo = Object.assign(files[i]);
                        fileInfo.fileExtention = '.' + files[i].name.split('.')[files[i].name.split('.').length - 1];
                        fileInfo.displayName = files[i].name;
                        fileInfo.index = i + startVal;
                        getBase64(fileInfo, i + startVal).then(function (data2) {
                            data.attachments[data2.counter] = data2.result;
                        });
                        data.fileInfo[i + startVal] = fileInfo;
                        data.empValue.push(fileInfo);
                    }
                };
                function getBase64(file, counter) {
                    var total = {counter: counter};
                    var defer = $.Deferred();
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        total.result = reader.result;
                        defer.resolve(total);
                    };
                    reader.onerror = function (error) {
                        defer.reject(error);
                    };
                    return $.when(defer);
                }
                self.getInputNumberValue = function (event, data) {
                    data.empValue = event.target.value;
                };
                self.handleEmpValue = function (empValue) {
                    return empValue == '' ? 0 : empValue
                }
                self.getNumeric = function (val) {
                    return val ? Number(val) : null;
                }
                self.eatNonNumbers = function (event) {
                    var charCode = (event.which) ? event.which : event.keyCode;
                    var char = String.fromCharCode(charCode);
                    var replacedValue = char.replace(/[^0-9\.]/g, '');
                    if (char !== replacedValue) {
                        event.preventDefault();
                    }
                };
                self.enabledApplayOnBehave = function (event) {
                    if (event.detail.value.length) {
                        $("#empDiv").removeClass("disable-div");
                        self.empRequired(true);
                    } else {
                        $("#empDiv").addClass("disable-div");
                        self.empRequired(false);
                    }
                }
                self.setRequiredClass = function (required, prompt, type = "") {
                    if (type == "") {
                        if (required == "1") {
                            return " required " + prompt;
                        } else {
                            return " " + prompt;
                        }
                    } else if (type == "signature") {
                        if (required == "1") {
                            return " required " + prompt;
                        } else {
                            return " " + prompt;
                        }
                    } else if (type == "formula") {
                        if (required == "1") {
                            return " required " + prompt;
                        } else {
                            return " " + prompt;
                        }
                }
                };
                var defaults = {};
                self.getDepend = function (event, data) {
                    setTimeout(function () {
                        console.log(data)
                        var createDepend = function (reqData) {
                            if (reqData) {
                                if (reqData.type == "value") {
                                    for (var item2 of reqData.data) {
                                        console.log(item2.prompt + " >> " + item2.value)
                                        $("." + item2.prompt).attr("value", item2.value);
                                        var arr2 = [];
                                        for (var item in self.requestSegments()) {
                                            if (self.requestSegments()[item].prompt == item2.prompt) {
                                                if (self.requestSegments()[item].type == "note") {
                                                    defaults[item.prompt] = self.requestSegments()[item].defaultValue;
                                                    self.requestSegments()[item].defaultValue = item.value;
                                                    self.requestSegments()[item].empValue = item.value;
                                                } else if (self.requestSegments()[item].type == "formula") {
                                                    defaults[item.prompt] = self.requestSegments()[item].formula;
                                                    self.requestSegments()[item].formula = item2.value;
                                                    self.requestSegments()[item].empValue = item2.value;
                                                } else if (self.requestSegments()[item].type == "html") {
                                                    defaults[item.prompt] = self.requestSegments()[item].formula;
                                                    self.requestSegments()[item].formula = item2.value;
                                                    self.requestSegments()[item].empValue = item2.value;
                                                } else {
                                                    self.requestSegments()[item].empValue = item2.value;
                                                }
                                            }
                                            arr2.push(self.requestSegments()[item]);
                                        }

                                        self.requestSegments([]);
                                        self.requestSegments(arr2);
                                    }
                                } else {
                                    var arr2 = []
                                    for (var item in self.requestSegments()) {
                                        if (self.requestSegments()[item].prompt == reqData.prompt) {
                                            self.requestSegments()[item].valueSets = reqData.value;
                                        }
                                        arr2.push(self.requestSegments()[item]);
                                    }

                                    self.requestSegments([]);
                                    self.requestSegments(arr2);
                                }
                            }

                        }


                        if (data.depend && data.empValue) {
                            if (app.compositUserInfo().empRequestsId) {
                                self.empId(app.compositUserInfo().personId)
                            }
                            console.log("self.empId >>> " + self.empId);
                            var personId = self.empId() != '' ? self.empId() : loggedUser.personId;
                            services.addGeneric('employeeRequest/getDepend?requestId=' + data.requestId + '&formulaId=' + data.depend + '&personId=' + personId + '&', self.requestSegments()).then(createDepend, function (xhr) {
                                app.failCbFn(xhr)
                            });
                        }
                    }, 10);
                }

                self.txt = function (event) {
                    if (event.detail.value) {
                        event.detail.value.label = event.detail.value.label.replace("##", "\t");
                    }
                }
                self.optionDraw = function (context, data) {
                    var elem = document.createElement('oj-option');
                    var span = document.createElement('span');
                    span.className = 'oj-listbox-highlighter-section';
                    if (context.data.label && context.data.label.includes("##")) {
                        span.innerHTML = context.data.label.replace("##", "&emsp;&emsp;&emsp;");
                    } else {
                        span.textContent = context.data.label;
                    }
                    elem.appendChild(span);
                    return elem;
                }
                self.setStarChart = function (required) {
                    if (required == "1") {
                        return " * ";
                    } else {
                        return "";
                    }
                };
                self.showRequiredInMobileMode = ko.computed(function () {
                    if (self.pageContentWidth() > 1000) {
                        self.test = function () {
                        }
                    } else {
                        self.test = function (event) {
                            if ($(event.target).parent().children('span').css('visibility') == 'visible') {
                                $(event.target).parent().children('span').css('visibility', 'hidden');
                            } else {
                                $(event.target).parent().children('span').css('visibility', 'visible');
                            }
                        }
                    }
                });
                self.delegate = ko.observable({});
                self.lookups = new ko.observable([]);
                self.redirectReq = function (mode) {
                    if (mode == "DLG") {
                        self.actionDialog("Delegation Dialog");
                    } else {

                        self.actionDialog("Reassign Dialog");
                    }
                    if (!self.lookups().userAdmin) {
                        listLookups();
                    }
                    document.getElementById("delegateDialog").open();
                    self.delegate({action: mode, personId: []});
                }
                function listLookups() {
                    var getLookups = function (data) {
                        data.userAdmin.unshift({label: '', value: ''})
                        self.lookups(data);
                    };
                    services.getGeneric('genericLookup/getLookup?lookupValueCode=userAdmin&').then(getLookups, app.failCbFn);
                }
                self.runDelegate = function () {
                    self.delegate().transactionId = app.compositUserInfo().empRequestsId;
                    self.delegate().step = app.compositUserInfo().step;
                    self.delegate().requestId = app.compositUserInfo().requestId;
                    self.delegate().requestName = app.compositUserInfo().requestName;
                    self.delegate().personName = app.compositUserInfo().userName;
                    services.addGeneric('employeeRequest/delegate?', self.delegate()).then(function () {
                        app.success_request("Request has been delegated successfully", function () {
                            app.navigator( 'dashboard');
                        });
                    }, function (xhr) {
                        app.failCbFn(xhr)
                    });
                }
                self.updateSegments = function () {
                    //var employeeId = $("#empDiv").val();
                    var employeeId = $("#empDiv").val() != '' ? $("#empDiv").val() : loggedUser.personId;
                    var requestId = app.router._activeState.params.id;
                    var step = 0;
                    var updatableSegments = {};
                    var stableSegments = [];
                    for (var item of self.requestSegments()) {
                        if (item.valueSetId || item.type == 'formula') {
                            updatableSegments[item.segmentId] = self.requestSegments().indexOf(item);
                        } else {
                            stableSegments.push(item)
                        }
                    }
                    var specificEmployeeSegements = function (data) {
                        for (var item of data) {
                            var itemIndex = updatableSegments[item.segmentId];
                            stableSegments.splice(itemIndex, 0, item);
                        }
                        self.requestSegments(stableSegments)
                    }
                    if (Object.keys(updatableSegments).length > 0 && employeeId) {
                        services.getGeneric('employeeRequest/specificEmployeeSegements?requestId=' + requestId + '&employeeId=' + employeeId + '&step=' + step + '&segments=' + Object.keys(updatableSegments).join(",") + '&').then(specificEmployeeSegements, app.failCbFn);
                    }
                }





            }

            return model;
        });

