define(['ojs/ojcore', 'text!./comp.html', './comp',
    'ojs/ojcomposite'], function (oj, view, viewModel) {
    oj.Composite.register('dynamic-form', {
        view: view,
        viewModel: viewModel,
        metadata: {
            "name": "dynamic-form",
            "description": "My custom link component",
            "version": "1.0.0",
            "properties": {
                "data": {
                    "type": "object"
                }
            }
        }
    });
}
);