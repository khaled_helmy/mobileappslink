/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['ServicesCalling/Services', 'ServicesCalling/servicePath'],
        function (serviceConfig, servicePath) {

            /**auther = mohiee
             * The view model for managing all services
             */
            function services() {

                var self = this;
                //var restPath="http://payrollsevencloud.com:7007/SSHR/resources/";
                //var restPath="http://130.61.180.179/SSHR/resources/";
                var restPath = servicePath.restPath;
                var projectPath = servicePath.projectPath;
                console.log(restPath);

                self.getSyncGeneric = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;

                    return serviceConfig.callGetSyncService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers);
                };
                // POST example
                self.getGeneric = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;

                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.getGenericServlet = function (servletName, headers) {
                    var serviceURL = "http://localhost:7101/SSHR/" + servletName;

                    return serviceConfig.callGetServletService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };

                self.addGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.editGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
                };
                self.deleteGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    var headers = {
                    };
                    return serviceConfig.callDeleteService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
                };
                self.getPath = function () {
                    return restPath;
                }

            }
            ;

            return new services();
        });