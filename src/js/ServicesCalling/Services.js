/**auther = mohiee**/

define(['jquery'],
        function ($) {
            function ServicesViewModel() {
                var self = this;
                self.contentTypeApplicationJSON = 'application/json';


                // get Sync 
                self.callGetSyncService = function (serviceUrl, contentType, headers) {
                    var defer = $.Deferred();
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';
                    $.ajax({
                        type: "GET",
                        url: serviceUrl + 'token=' + token,
                        headers: headers,

                        success: function (data) {
                            defer.resolve(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            defer.reject(xhr);
                        }
                    });

                    return $.when(defer);
                };
//call get ws using ajax
                self.callGetService = function (serviceUrl, contentType, headers, asynchronized) {
                    var defer = $.Deferred();
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';
                    $('#loader').show();
                    $.ajax({
                        type: "GET",
//                        async: false,
                        url: serviceUrl + 'token=' + token,
                        headers: headers,

                        success: function (data) {
                            $('#loader').hide();
                            defer.resolve(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#loader').hide();
                            defer.reject(xhr);
                        }
                    });

                    return $.when(defer);
                };

                self.callGetServletService = function (serviceUrl, contentType, headers, asynchronized) {
                    var defer = $.Deferred();
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';
                    $('#loader').show();
                    $.ajax({
                        type: "GET",
//                        async: false,
                        url: serviceUrl + 'token=' + token,
                        headers: headers,

                        success: function (data) {
                            $('#loader').hide();
                            defer.resolve(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#loader').hide();
                            defer.reject(xhr);
                        }
                    });

                    return $.when(defer);
                };
                //call post ws using ajax
                self.callPostService = function (serviceUrl, payload, contentType, asynchronized, headers) {
                    var payloadStr = '';
                    if (typeof payload === 'string' || payload instanceof String) {
                        payloadStr = payload;
                    } else {
                        payloadStr = JSON.stringify(payload);
                    }

                    var defer = $.Deferred();
                    console.log(payloadStr);
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';
                    $('#loader').show();

                    $.ajax({
                        type: "POST",
//                        async: asynchronized,
                        headers: headers,
                        url: serviceUrl + 'token=' + token,
                        contentType: contentType,
                        data: payloadStr,
                        success: function (data, status, request) {
                            //console.log("data"+ data);         
                            $('#loader').hide();

                            defer.resolve(data, status, request);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#loader').hide();

                            defer.reject(xhr);
                        }
                    });
                    return $.when(defer);
                };
                //call put ws using ajax
                self.callPutService = function (serviceUrl, payload, contentType, asynchronized, headers) {
                    var payloadStr = '';
                    if (typeof payload === 'string' || payload instanceof String) {
                        payloadStr = payload;
                    } else {
                        payloadStr = JSON.stringify(payload);
                    }

                    var defer = $.Deferred();
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';

                    $.ajax({
                        type: "PUT",
//                        async: asynchronized,
                        headers: headers,

                        url: serviceUrl + 'token=' + token,
                        contentType: contentType,
                        data: payloadStr,
                        success: function (data) {
                            defer.resolve(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            defer.reject(xhr);
                        }
                    });
                    return $.when(defer);
                };
                // cal DELETE ws using ajax
                self.callDeleteService = function (serviceUrl, payload, contentType, asynchronized, headers) {
                    var payloadStr = '';
                    if (typeof payload === 'string' || payload instanceof String) {
                        payloadStr = payload;
                    } else {
                        payloadStr = JSON.stringify(payload);
                    }

                    var defer = $.Deferred();
                    var token = sessionStorage['token'] && sessionStorage['token'] !== 'undefined' ? sessionStorage['token'] : '';

                    $.ajax({
                        type: "DELETE",
//                        async: asynchronized,
                        headers: headers,

                        url: serviceUrl + 'token=' + token,
                        contentType: contentType,
                        data: payloadStr,
                        success: function (data) {
                            defer.resolve(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            defer.reject(xhr);
                        }
                    });
                    return $.when(defer);
                };
            }
            ;

            return new ServicesViewModel();
        }
);