
define([],
        function () {
            function ServicesViewModel() {
                var self = this;
                
                self.projectPath = "dev.payrollsevencloud.com:7008/PORTAL7_BE";
                self.restPath = "https://" + self.projectPath + "/resources/";
//                
//                self.projectPath = "localhost:7101/PORTAL7_BE";
//                self.restPath = "http://" + self.projectPath + "/resources/";
            }
            return new ServicesViewModel();
        }
);