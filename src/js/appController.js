/**
 * @license
 * Copyright (c) 2014, 2021, Oracle and/or its affiliates.
 * Licensed under The Universal Permissive License (UPL), Version 1.0
 * as shown at https://oss.oracle.com/licenses/upl/
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['knockout', 'ojs/ojcontext', 'ServicesCalling/servicesConsume', 'ojs/ojmodule-element-utils', 'ojs/ojknockouttemplateutils', 'ojs/ojcorerouter', 'ojs/ojmodulerouter-adapter', 'ojs/ojknockoutrouteradapter', 'ojs/ojurlparamadapter', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'ojs/ojarraydataprovider',
    'ojs/ojoffcanvas', 'ojs/ojmodule-element', 'ojs/ojknockout'],
        function (ko, Context, services, moduleUtils, KnockoutTemplateUtils, CoreRouter, ModuleRouterAdapter, KnockoutRouterAdapter, UrlParamAdapter, ResponsiveUtils, ResponsiveKnockoutUtils, ArrayDataProvider, OffcanvasUtils) {

            function ControllerViewModel() {
                var self = this;
                this.KnockoutTemplateUtils = KnockoutTemplateUtils;
                self.token = ko.observable();
                self.logo = ko.observable();
                self.isManager = ko.observable();
                self.isUpdateRole = ko.observable();
                self.isHr = ko.observable();
                self.isIt = ko.observable();
                self.user = ko.observable();
                self.trns = ko.observable({})
                self.pageTitle = ko.observable("")
                // Handle announcements sent when pages change, for Accessibility.
                self.compositUserInfo = ko.observable({});
                self.compositDate = ko.observable([]);
                this.manner = ko.observable('polite');
                this.message = ko.observable();
                announcementHandler = (event) => {
                    this.message(event.detail.message);
                    this.manner(event.detail.manner);
                };

                document.getElementById('globalBody').addEventListener('announce', announcementHandler, false);

                self.logout = function (event) {
                    localStorage.removeItem('user');
                    sessionStorage.removeItem('token');
                    self.token("");
                    self.isManager("");
                    self.isUpdateRole("");
                    self.isHr("");
                    self.isIt("");
                    self.user("");
                }
                // Media queries for repsonsive layouts
                const smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                this.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                const mdQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                this.mdScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                let navData = [
                    {path: '', redirect: 'dashboard'},
                    {path: 'dashboard', detail: {label: 'Dashboard', iconClass: 'icofont icofont-dashboard', hidden: false}},
                    {path: 'requests', detail: {label: 'Requests', iconClass: 'fa fa-chart', hidden: true}},
                    {path: 'new', detail: {label: 'New Request', iconClass: 'oj-ux-ico-information-s', hidden: true}},
                    {path: 'login', detail: {label: 'Logout', iconClass: 'icofont icofont-logout', hidden: false}},
                    {path: 'ss_dynamic', detail: {label: 'createdRequests', iconClass: 'oj-ux-ico-information-s', hidden: true}},
                    {path: 'details', detail: {label: 'Details', iconClass: 'oj-ux-ico-information-s', hidden: true}},
                ];
                self.selectLabelValue = function (myArray = [], nameKey) {
                    for (var i = 0; i < myArray.length; i++) {
                        if (typeof nameKey !== 'undefined' && typeof myArray[i].value !== 'undefined') {
                            if (myArray[i].value.toString() === nameKey.toString()) {
                                return myArray[i].label;
                            }
                        } else {
                            return nameKey;
                        }
                }
                }
                if (sessionStorage['token']) {
                    navData[0].redirect = 'dashboard';
                    var user_detials = JSON.parse(localStorage['user'])['Login Details'];
                    self.isManager(user_detials.userJobs.includes(user_detials.SS_MANAGER));
                    self.isUpdateRole(user_detials.userJobs.includes(user_detials.SS_UPDATE_ROLE));
                    self.isHr(user_detials.userJobs.includes(user_detials.SS_HR));
                    self.isIt(user_detials.userJobs.includes(user_detials.SS_IT));

                } else {
                    self.logout();
                    navData[0].redirect = 'login';
                    var user_detials = {};
                }


                // Router setup
                let router = new CoreRouter(navData, {
                    urlAdapter: new UrlParamAdapter()
                });
                router.sync();
                self.router = router;
                this.moduleAdapter = new ModuleRouterAdapter(router);

                this.selection = new KnockoutRouterAdapter(router);

                // Setup the navDataProvider with the routes, excluding the first redirected
                // route.
                this.navDataProvider = new ArrayDataProvider(navData.slice(1), {keyAttributes: "path"});

                // Drawer
                // Close offcanvas on medium and larger screens
                this.mdScreen.subscribe(() => {
                    OffcanvasUtils.close(this.drawerParams);
                });
                this.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                // Called by navigation drawer toggle button and after selection of nav drawer item
                this.toggleDrawer = () => {
                    this.navDrawerOn = true;
                    return OffcanvasUtils.toggle(this.drawerParams);
                }

                // Header
                // Application Name used in Branding Area
                this.appName = ko.observable("App Name");
                // User Info used in Global Navigation area
                this.userLogin = ko.observable("john.hancock@oracle.com");

                // Footer
                this.footerLinks = [
                    {name: 'About Oracle', linkId: 'aboutOracle', linkTarget: 'http://www.oracle.com/us/corporate/index.html#menu-about'},
                    {name: "Contact Us", id: "contactUs", linkTarget: "http://www.oracle.com/us/corporate/contact/index.html"},
                    {name: "Legal Notices", id: "legalNotices", linkTarget: "http://www.oracle.com/us/legal/index.html"},
                    {name: "Terms Of Use", id: "termsOfUse", linkTarget: "http://www.oracle.com/us/legal/terms/index.html"},
                    {name: "Your Privacy Rights", id: "yourPrivacyRights", linkTarget: "http://www.oracle.com/us/legal/privacy/index.html"},
                ];

                self.getLoginCBFN = function (data) {
                    if (data['Login Details'].error == "FALSE") {
                        $('.main_layout').show();
                        localStorage['user'] = JSON.stringify(data);
                        sessionStorage['token'] = data['Login Details'].token;
                        self.token(data['Login Details'].token);
                        self.logo(data['Login Details'].logo);
                        self.isManager(data['Login Details'].userJobs.includes(data['Login Details'].SS_MANAGER));
                        self.isUpdateRole(data['Login Details'].userJobs.includes(data['Login Details'].SS_UPDATE_ROLE));
                        self.isHr(data['Login Details'].userJobs.includes(data['Login Details'].SS_HR));
                        self.isIt(data['Login Details'].userJobs.includes(data['Login Details'].SS_IT));
                        self.user(data['Login Details']);
                        router.go({path: 'dashboard'})
                    } else {
                        $('.main_layout').hide();
                        router.go({path: '/login'})
//                        var getTranslation = oj.Translations.getTranslatedString;
//                        self.createErrMsg(getTranslation("loginError"));
                    }
                }
                self.loginRequest = function (username, password) {
                    if (password) {
                        localStorage["password"] = password;
                    } else {
                        password = localStorage["password"];
                    }
                    var domain = 'time7';
                    var lang = localStorage["selectedLanguage"] ? localStorage["selectedLanguage"] : "US";
                    var body = {};
                    body["userName"] = username;
                    body["password"] = password;
                    body["siteName"] = domain;
                    body["Lang"] = lang;
                    services.addGeneric('login?', body).then(self.getLoginCBFN, self.failCbFn);
                }
                self.dataConverterToShow = function (dateTime) {
                    var str_month = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
                    if (dateTime) {
                        var date = dateTime.split(" ")[0]
                        var dataSpt = date.split(date.includes("-") ? "-" : "/");
                        if (!isNaN(dataSpt[1])) {
                            return dataSpt[2] + "-" + dataSpt[1] + "-" + dataSpt[0];
                        } else {
                            return dataSpt[0] + "-" + months[str_month.indexOf(dataSpt[1])] + "-20" + dataSpt[2];
                        }
                    } else {
                        return " ";
                    }
                }

                self.compositDate = ko.observable([]);
                self.compositEmpDate = ko.observable({});
                self.compositRequestId = ko.observable("");
                self.dycReqs = new ko.observable({});
                self.compComment = ko.observable("");
                self.canAppyOnBehave = ko.observable();
                self.compositIconDate = ko.observable([]);
                self.compositUserInfo = ko.observable({});
                self.templateId = ko.observable("");
                self.compActionType = ko.observable('add');

                self.checkRequest = function () {
                    return true;
                }

                self.getUserInfo = function () {
                    return JSON.parse(localStorage['user']);
                }
                self.SegTableValues = new ko.observable({});

                self.returnSelected = function (value, optionVal) {
                    var selected = "false";
                    if (optionVal == value) {
                        selected = "selected";
                    }
                    return selected;

                }
                self.promisedRequests = function (refreshCallback) {
                    //console.log("checklogin")
                    if (oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME) !== null) {
                        self.dateConverter(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).createConverter({
                            pattern: 'dd-MM-yyyy'
                        }));
                    }
//                    self.handleFusionStyle(sessionStorage['selectedTheme']);
                    $('.main_layout').show();
//                    var lgn = self.loginCheck();
//                    if (lgn == 'logined') {
//                        //setExpDate()
//                        //console.log("loggedin1")
//                        self.drawMenu(JSON.parse(localStorage['user'])['Pages Roles']);
//                        refreshCallback();
//                    } else {
//                        //console.log("loggedin2")
//
//                        var url = new URL(window.location.href);
//                        sessionStorage['rootDirection'] = url.searchParams.get("root");
//                        if (lgn) {
//                            lgn.then(function (data) {
//                                self.getLoginCBFN(data);
//                                self.drawMenu(JSON.parse(localStorage['user'])['Pages Roles']);
//                                refreshCallback();
//                            });
//                        }
//                    }
                    if ($('#pcoded').attr('vertical-nav-type') == 'expanded') {
                        document.getElementById('mobile-collapse').click();
                    }
                }
                self.navigator = function (page, data = {}) {
                    router.go({path: page, params: data})
                }
                
                self.close_msg = function () {
                    $("#msg_modal").hide();
                    $("#background").hide();
                }
                self.success_request = function (msg, refreshCallback) {
                    $(".sa-success").show();
                    $("#customErrorMassege").hide();
                    if (localStorage["selectedLanguage"] == 'US') {
                        $("#msg").text("Success");
                    } else {
                        $("#msg").text("تم بنجاح");
                    }
                    ;
                    //                    $(".sa-info").hide();
                    //                    $(".sa-error").hide();
                    //                    $(".sa-warning").hide();
                    $("#sub_msg").text(msg);
                    $("#msg_modal").show();
                    $("#background").show();
                    refreshCallback();
                }
                self.close_msg = function () {
                    $("#msg_modal").hide();
                    $("#background").hide();
                }

                self.failModalConfirm = function (callback, title, msgText) {
                    $(".fail-confirm-modal-title").text(title);
                    $(".fail-confirm-modal-text").text(msgText);
                    $('#fail-confirm-modal').show();
                    $("#background").show();
                    $("#fail-modal-btn-no").one("click", function (e) {
                        $("#fail-modal-btn-yes").off();
                        $("#fail-confirm-modal").hide();
                        $("#background").hide();
                        callback(false);
                    });
                    $("#fail-modal-btn-yes").one("click", function (e) {
                        $("#fail-modal-btn-no").off();
                        $("#fail-confirm-modal").hide();
                        $("#background").hide();
                        callback(true);
                    });
                };


                self.failCbFn = function (xhr, failFun = function() {}) {
                    //console.log(xhr.responseText);
                    var responseText = xhr.responseText.split("#");
                    $(".sa-success").hide();
                    $("#customErrorMassege").show();
                    if (responseText.length < 3) {
                        $("#customErrorMassege").attr("class", "sa-icon sa-info");
                        if (xhr.responseText != "Permission <br/> Denied") {
                            $("#msg").text("Technical Error Occurred");
                            $("#sub_msg").text(xhr.responseText);
                        } else {
                            $("#msg").html(xhr.responseText);
                            $("#sub_msg").text("");
                        }
                        $("#msg_modal").show();
                        $("#background").show();
                    } else {
                        failFun(responseText)
                    }
                };
            }
            // release the application bootstrap busy state
            Context.getPageContext().getBusyContext().applicationBootstrapComplete();

            return new ControllerViewModel();
        }
);
